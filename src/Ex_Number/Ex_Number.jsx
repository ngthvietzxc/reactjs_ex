import React, { Component } from "react";

export default class Ex_Number extends Component {
  state = {
    number: 0,
  };
  handleChangeNumber = (soLuong) => {
    this.setState({
      number: this.state.number + soLuong,
    });
  };
  render() {
    return (
      <div>
        <button
          onClick={() => {
            this.handleChangeNumber(-1);
          }}
          className="btn btn-danger"
        >
          -
        </button>
        <button className="btn">{this.state.number}</button>
        <button
          onClick={() => {
            this.handleChangeNumber(+1);
          }}
          className="btn btn-primary"
        >
          +
        </button>
      </div>
    );
  }
}
