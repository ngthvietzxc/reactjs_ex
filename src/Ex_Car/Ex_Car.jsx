import React, { Component } from "react";

export default class Ex_Car extends Component {
  state = {
    imgSrc: "./Car_IMG/CarBasic/products/red-car.jpg",
  };
  handleChangerColor = (color) => {
    let newImg = `./Car_IMG/CarBasic/products/${color}-car.jpg`;
    this.setState({ imgSrc: newImg });
  };
  render() {
    return (
      <div className="container row">
        <img className="col-6" src={this.state.imgSrc} alt="" />
        <div className="col-6 pt-5">
          <button
            onClick={() => {
              this.handleChangerColor("red");
            }}
            className="btn border"
          >
            Red
          </button>
          <button
            onClick={() => {
              this.handleChangerColor("black");
            }}
            className="btn border mx-5"
          >
            Black
          </button>
          <button
            onClick={() => {
              this.handleChangerColor("silver");
            }}
            className="btn border"
          >
            Silver
          </button>
        </div>
      </div>
    );
  }
}
