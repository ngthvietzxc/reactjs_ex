import React, { Component } from "react";
import { movieArr } from "./DataMovie";

export default class RenderWithMap extends Component {
  state = {
    movieDetail: movieArr[0],
  };
  handleChangeDetail = (movie) => {
    this.setState({ movieDetail: movie });
  };
  renderMovieList = () => {
    let movieList = movieArr.map((item) => {
      console.log("🚀 ~ RenderWithMap ~ movieList ~ item:", item);
      return (
        <div
          className="card col-2 m-5"
          style={{ width: "18rem", fontFamily: "roboto" }}
        >
          <img className="justify-content-cente m-2" src={item.hinhAnh} />
          <div classname="card-body">
            <h5 classname="card-title">{item.tenPhim}</h5>
            <button
              onClick={() => {
                this.handleChangeDetail(item);
              }}
              className="btn btn-primary"
            >
              Xem ngay
            </button>
          </div>
        </div>
      );
    });
    return movieList;
  };
  render() {
    return (
      <div>
        <div>Name: {this.state.movieDetail.tenPhim}</div>
        <div className="row">{this.renderMovieList()}</div>
      </div>
    );
  }
}
