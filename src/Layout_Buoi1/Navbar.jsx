import React, { Component } from "react";

class Navbar extends Component {
  render() {
    return (
      <div>
        { /* Responsive navbar*/ }
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container px-lg-5 justify-content-between align-items-center ">
            <a className="navbar-brand d-flex justify-content-start" href="#!">
              Start Bootstrap
            </a>
            <div
              className="collapse navbar-collapse d-flex justify-content-end"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <a className="nav-link active" aria-current="page" href="#!">
                    Home
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#!">
                    About
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#!">
                    Contact
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;