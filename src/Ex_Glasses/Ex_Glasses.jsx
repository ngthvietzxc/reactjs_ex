import React, { Component } from "react";
import { glassesArr } from "./DataGlasses";
export default class Ex_Glasses extends Component {
  state = {
    glassesDetail: glassesArr[0],
  };
  handleChangeDetail = (glasses) => {
    this.setState({ glassesDetail: glasses });
  };
  renderGlassesList = () => {
    let glassesList = glassesArr.map((item) => {
      return (
        <div className="row col-2">
          <div className="card m-4">
            <img
              onClick={() => {
                this.handleChangeDetail(item);
              }}
              src={item.hinhAnh}
            />
          </div>
        </div>
      );
    });
    return glassesList;
  };
  render() {
    return (
      <div>
        <div
          className="p-4 text-uppercase text-white text-lg font-weight-bold"
          style={{
            backgroundColor: "rgba(0, 0, 0, 0.8)",
          }}
        >
          Try Glasses App Online
        </div>
        <div className="container">
          <div className="row px-2" style={{ position: "relative" }}>
            <div className="col mx-2">
              <div
                style={{
                  height: "70vh",
                  backgroundRepeat: "no-repeat",
                  backgroundImage: 'url("./Glasses/glassesImage/model.jpg")',
                }}
              >
                <img src={this.state.glassesDetail.hinhAnh} alt="" />
                <div style={{ position: "absolute", bottom: 10 }}>
                  <div>{this.state.glassesDetail.name}</div>
                  <div>{this.state.glassesDetail.desc}</div>
                </div>
              </div>
            </div>
            <div className="col mx-2">
              <div
                style={{
                  height: "70vh",
                  backgroundRepeat: "no-repeat",
                  backgroundImage: 'url("./Glasses/glassesImage/model.jpg")',
                }}
              ></div>
            </div>
          </div>
        </div>

        <div className="justify-content-center row">
          {this.renderGlassesList()}
        </div>
      </div>
    );
  }
}
