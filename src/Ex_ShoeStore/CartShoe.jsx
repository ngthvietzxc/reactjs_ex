import React, { Component } from "react";

export default class CartShoe extends Component {
  renderContentCart = () => {
    return this.props.cartShoe.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img style={{ width: 80 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-default border"
            >
              -
            </button>
            <button style={{ width: 50 }} className="btn btn-default  border">
              {item.soLuong}
            </button>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
              className="btn btn-default border"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
              className="btn btn-default border"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <div>
          <table className="table border">
            <thead>
              <tr>
                <th className="border">ID</th>
                <th className="border">Name</th>
                <th className="border">Price</th>
                <th className="border">Image</th>
                <th className="border">Quantity</th>
                <th className="border">Action</th>
              </tr>
            </thead>
            <tbody>{this.renderContentCart()}</tbody>
          </table>
        </div>
      </div>
    );
  }
}
