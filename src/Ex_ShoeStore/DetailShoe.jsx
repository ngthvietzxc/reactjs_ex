import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { name, price, description, shortDecription, quantity, image } =
      this.props.detailShoe;
    return (
      <div>
        <div
          className="card"
          style={{
            borderRadius: "4px",
            boxShadow:
              "rgba(50, 185, 205, 0.25) 0px 50px 100px -20px, rgba(255, 255, 255, 0.3) 0px 30px 60px -30px",
          }}
        >
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{price}</p>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">{quantity}</li>
            <li className="list-group-item">{description}</li>
            <li className="list-group-item">{shortDecription}</li>
          </ul>
        </div>
      </div>
    );
  }
}
/**{
  "dataShoe": {
    "id": 1,
    "name": "Adidas Prophere",
    "alias": "adidas-prophere",
    "price": 350,
    "description": "The adidas Primeknit upper wraps the foot with a supportive fit that enhances movement.\r\n\r\n",
    "shortDescription": "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
    "quantity": 995,
    "image": "http://svcy3.myclass.vn/images/adidas-prophere.png"
  }
} */
