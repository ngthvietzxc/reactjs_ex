import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.list.map((item, index) => {
      return (
        <ItemShoe
          handleAddToCart={this.props.handleAddToCart}
          handleOnClickDetail={this.props.handleViewDetail}
          dataShoe={item}
          key={index}
        />
      );
    });
  };
  render() {
    return <div className="row p-5">{this.renderListShoe()}</div>;
  }
}
