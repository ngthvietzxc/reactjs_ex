import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    return (
      <div className="col-6">
        <div
          className="card text-left m-2"
          style={{ boxShadow: "1px 2px 9px #999" }}
        >
          <img className="card-img-top" src={this.props.dataShoe.image} />
          <div className="card-body">
            <h4 className="card-title">{this.props.dataShoe.name}</h4>
            <h3 className="card-text">{this.props.dataShoe.price}</h3>
            <div>
              <button
                onClick={() => {
                  this.props.handleAddToCart(this.props.dataShoe);
                }}
                className="float-right btn border text-white bg-dark"
              >
                Buy
              </button>
              <button
                onClick={() => {
                  this.props.handleOnClickDetail(this.props.dataShoe);
                }}
                className="float-right btn border text-white bg-dark"
              >
                View Details
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
/**{
  "dataShoe": {
    "id": 1,
    "name": "Adidas Prophere",
    "alias": "adidas-prophere",
    "price": 350,
    "description": "The adidas Primeknit upper wraps the foot with a supportive fit that enhances movement.\r\n\r\n",
    "shortDescription": "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
    "quantity": 995,
    "image": "http://svcy3.myclass.vn/images/adidas-prophere.png"
  }
} */
