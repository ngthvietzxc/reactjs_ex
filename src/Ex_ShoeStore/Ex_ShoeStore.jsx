import React, { Component } from "react";
import CartShoe from "./CartShoe";
import { shoeArr } from "./DataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeStore extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    cartShoe: [],
  };
  handleChangeDetail = (shoe) => {
    this.setState({ detailShoe: shoe });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cartShoe];
    // TH1: Sản phẩm chưa có trong giỏ hàng => tạo object mới từ thông tin object cũ, có thêm key số lượng 1 (có push)
    // TH2: Sản phẩm đã có trong giỏ hàng => update key số lượng (không push)
    let index = cloneCart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index === -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({ cartShoe: cloneCart });
  };
  handleDelete = (id) => {
    let cloneCart = [...this.state.cartShoe];
    let index = cloneCart.findIndex((shoe) => {
      return shoe.id === id;
    });
    cloneCart.splice(index, 1);
    this.setState({ cartShoe: cloneCart });
  };
  handleChangeQuantity = (id, luaChon) => {
    let cloneCart = [...this.state.cartShoe];
    let index = cloneCart.findIndex((shoe) => {
      return shoe.id === id;
    });
    cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
    cloneCart[index].soLuong === 0 && cloneCart.splice(index, 1);
    // ** cloneCart[index].soLuong => true thì chạy splice, false thì dừng
    this.setState({ cartShoe: cloneCart });
  };
  render() {
    return (
      <div>
        <div className="row" style={{ display: "flex" }}>
          <div style={{ width: "45%" }}>
            <div className="col-12">
              <ListShoe
                handleViewDetail={this.handleChangeDetail}
                list={this.state.shoeArr}
                handleAddToCart={this.handleAddToCart}
              />
            </div>
          </div>
          <div style={{ width: "55%", paddingTop: 60, paddingRight: 60 }}>
            <CartShoe
              handleChangeQuantity={this.handleChangeQuantity}
              handleDelete={this.handleDelete}
              cartShoe={this.state.cartShoe}
            />
            <div style={{ padding: 100 }}>
              <DetailShoe detailShoe={this.state.detailShoe} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
