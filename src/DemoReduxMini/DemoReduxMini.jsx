import React, { Component } from "react";
import { connect } from "react-redux";

class DemoReduxMini extends Component {
  render() {
    console.log("props", this.props);
    return (
      <div className="p-4">
        <button
          onClick={() => {
            this.props.handleGiam(1);
          }}
          className="btn btn-danger"
        >
          -
        </button>
        <button className="btn">{this.props.currentNumber}</button>
        <button onClick={this.props.handleTang} className="btn btn-primary">
          +
        </button>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    currentNumber: state.soLuong.number,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTang: () => {
      let action = {
        type: "TANG_SO_LUONG",
      };
      dispatch(action);
    },
    //
    handleGiam: (number = 1) => {
      let action = {
        type: "GIAM_SO_LUONG",
        payload: number,
      };
      dispatch(action);
    },
  };
};
// key => props
// value: giá trị trên state
export default connect(mapStateToProps, mapDispatchToProps)(DemoReduxMini);
