let initialState = { number: 1 };
export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    //
    case "TANG_SO_LUONG": {
      console.log("yes");
      state.number = state.number + 1;
      return { ...state };
    }
    case "GIAM_SO_LUONG": {
      state.number = state.number - action.payload;
      return { ...state };
    }

    default: {
      return state;
    }
  }
};
