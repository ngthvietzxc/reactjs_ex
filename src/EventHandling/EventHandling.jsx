import React, { Component } from "react";

export default class EventHandling extends Component {
  // state quản lý các data liên quan đến layout
  state = {
    username: "alice",
  };
  // Dùng setState đế update state, sau khi setState tự động chạy lại render giao diện
  username = "alice";
  handleChangeUsername = () => {
    let value = this.state.username === "Bob" ? "Alice" : "Bob";
    this.username = "bob";
    console.log("🚀 ~ this.username", this.username);
    this.setState({
      username: value,
    });
  };
  render() {
    return (
      <div>
        <h2>EventHandling</h2>
        <h4
          className={
            this.state.username === "Bob" ? "text-success" : "text-primary"
          }
        >
          Username: {this.state.username}
        </h4>
        <button onClick={this.handleChangeUsername} className="btn btn-danger">
          Change Username
        </button>
      </div>
    );
  }
}
