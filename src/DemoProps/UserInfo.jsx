import React, { Component } from "react";

export default class UserInfo extends Component {
  render() {
    console.log(this.props);
    let { name, age } = this.props;
    return (
      <div className="p-5 bg-warning">
        <h2>Username: {name}</h2>
        <h3>Age: {age}</h3>
        <button onClick={this.props.handleOnClick} className="btn btn-danger">
          Change Username
        </button>
      </div>
    );
  }
}
