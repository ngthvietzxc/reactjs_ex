import React, { Component } from "react";
import UserInfo from "./UserInfo";

export default class DemoProps extends Component {
  state = {
    username: "Alice",
    age: 22,
  };
  //   State ở đâu thì setState ở Component đó
  handleChangeUsername = () => {
    this.setState({
      username: "Bob" + Math.random(),
    });
  };
  render() {
    return (
      <div className="p-5 bg-success">
        <UserInfo
          name={this.state.username}
          age={this.state.age}
          handleOnClick={this.handleChangeUsername}
        />
      </div>
    );
  }
}
