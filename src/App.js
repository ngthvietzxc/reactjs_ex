import "./App.css";
import DemoReduxMini from "./DemoReduxMini/DemoReduxMini";
import Ex_ShoeStore from "./Ex_ShoeStore/Ex_ShoeStore";
// import DemoProps from "./DemoProps/DemoProps";
// import Ex_Glasses from "./Ex_Glasses/Ex_Glasses";
// import EventHandling from "./EventHandling/EventHandling";
// import Ex_Car from "./Ex_Car/Ex_Car";
// import Ex_Number from "./Ex_Number/Ex_Number";
// import RenderWithMap from "./RenderWithMap/RenderWithMap";

function App() {
  return (
    <div className="App">
      {/* Buổi 1 */}
      {/* Buổi 2 */}
      {/* <EventHandling /> */}
      {/* <Ex_Number /> */}
      {/* <Ex_Car /> */}
      {/* <RenderWithMap /> */}
      {/* <Ex_Glasses />*/}
      {/* Buổi 3: Props */}
      {/* <DemoProps /> */}
      {/* <Ex_ShoeStore /> */}
      {/* Buổi 5,6: Redux */}
      <DemoReduxMini />
    </div>
  );
}

export default App;
